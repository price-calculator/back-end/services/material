namespace Material.API;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using ServiceCollectionExtensions;

public class Startup
{
    public Startup(IConfiguration configuration) => Configuration = configuration;

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services) =>
        services
            .AddCustomControllers()
            .AddSwagger()
            .AddCustomServices()
            .AddDbContext()
            .AddAuthenticationBuilder()
            .AddIntegrationServices()
            .AddEventBus();

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app)
    {
        app.UseSwagger(c =>
        {
            c.RouteTemplate = "api/materials/swagger/{documentname}/swagger.json";
        });
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/api/materials/swagger/v1/swagger.json", "Material.API v1");
            c.RoutePrefix = "api/materials/swagger";
        });

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
