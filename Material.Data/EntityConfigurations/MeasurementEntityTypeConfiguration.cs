namespace Material.Data.EntityConfigurations;

using Domain.Models.MaterialAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class MeasurementEntityTypeConfiguration : IEntityTypeConfiguration<MeasurementUnit>
{
    public void Configure(EntityTypeBuilder<MeasurementUnit> builder)
    {
        builder.ToTable("measurements");

        builder.HasKey(o => o.Id);

        builder.Property<int>(o => o.Id)
            .ValueGeneratedNever()
            .IsRequired();

        builder.Property<string>(o => o.Name)
            .HasMaxLength(256)
            .IsRequired();

        builder.HasIndex(p => new { p.Name });
    }
}
