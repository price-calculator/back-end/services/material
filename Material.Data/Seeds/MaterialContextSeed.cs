namespace Material.Data.Seeds;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Data.Context;

using Domain.Models.MaterialAggregate;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;

using Polly;
using Polly.Retry;

public class MaterialContextSeed
{
    public async Task SeedAsync(MaterialContext context, ILogger<MaterialContextSeed> logger)
    {
        var policy = CreatePolicy(logger, nameof(MaterialContextSeed));

        await policy.ExecuteAsync(async () =>
            {
                if (!context.MeasurementUnits.Any())
                {
                    var measurementUnits = GetPreconfiguredMeasurementUnits();
                    await context.MeasurementUnits.AddRangeAsync(measurementUnits);
                    await context.SaveChangesAsync();
                }
            });
    }

    private static IEnumerable<MeasurementUnit> GetPreconfiguredMeasurementUnits()
    {
        return new List<MeasurementUnit>()
        {
            MeasurementUnit.Grams,
            MeasurementUnit.Milliliters,
            MeasurementUnit.Units
        };
    }

    private static AsyncRetryPolicy CreatePolicy(ILogger<MaterialContextSeed> logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().
            WaitAndRetryAsync(
                retryCount: retries,
                sleepDurationProvider: _ => TimeSpan.FromSeconds(5),
                onRetry: (exception, timeSpan, retry, context) =>
                {
                    logger.LogWarning(
                        exception,
                        "[{prefix}] Exception {exceptionType} with message {exceptionMessage} detected on attempt {retry} of {retries}",
                        prefix, exception.GetType(), exception.Message, retry, retries);
                });
    }
}
