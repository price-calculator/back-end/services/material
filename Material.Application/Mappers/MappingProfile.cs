namespace Material.Application.Mappers;

using AutoMapper;

using Domain.Models.MaterialAggregate;

using Handlers.Commands;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<CreateCommand, MaterialEntity>()
            .ForMember(me => me.MeasurementUnit, option => option.Ignore())
            .ConstructUsing(s =>
                new MaterialEntity(
                    s.Name,
                    MeasurementUnit.FromDisplayName<MeasurementUnit>(s.MeasurementUnit).Id,
                    s.MeasurementValue,
                    s.Price));
    }
}
