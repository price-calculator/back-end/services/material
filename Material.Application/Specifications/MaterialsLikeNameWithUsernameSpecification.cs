namespace Material.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.MaterialAggregate;

public class MaterialsLikeNameWithUsernameSpecification : BaseSpecification<MaterialEntity>
{
    public MaterialsLikeNameWithUsernameSpecification(string word, string username)
        : base(m => m.Name.Contains(word) && m.UserName == username) => AddInclude(m => m.MeasurementUnit);

}
