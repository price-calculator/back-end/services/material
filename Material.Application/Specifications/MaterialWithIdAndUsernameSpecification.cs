namespace Material.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.MaterialAggregate;

public class MaterialWithIdAndUsernameSpecification : BaseSpecification<MaterialEntity>
{
    public MaterialWithIdAndUsernameSpecification(int id, string username)
        : base(m => m.Id == id && m.UserName == username) =>
            AddInclude(m => m.MeasurementUnit);
}
