namespace Material.Application.Handlers.Commands;

using MediatR;

public class DeleteCommand : IRequest<Unit>
{
    public int Id { get; }

    public DeleteCommand(int id) => Id = id;
}
