namespace Material.Application.Models;

public class PaginationParams
{
    public int Skip { get; }
    public int Take { get; }

    public PaginationParams(int skip, int take)
    {
        Skip = skip;
        Take = take;
    }
}
