namespace Material.Application.Models;

public class GetAllQueryResponse
{
    public MaterialDTO[] Materials { get; set; }
    public int Total { get; set; }

    public GetAllQueryResponse(MaterialDTO[] materials, int total)
    {
        Materials = materials;
        Total = total;
    }
}
