namespace Material.Application.Validators;
using FluentValidation;

using Handlers.Commands;

public class CreateCommandValidator : AbstractValidator<CreateCommand>
{
    public CreateCommandValidator()
    {
        RuleFor(c => c.Name).NotEmpty().WithMessage("Name is required");
        RuleFor(c => c.MeasurementUnit).NotEmpty().WithMessage("MeasurementUnit is required");
        RuleFor(c => c.MeasurementValue).NotEmpty().GreaterThan(0).WithMessage("MeasurementValue must be greater than 0");
        RuleFor(c => c.Price).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Price must be greater than or equal to 0");
    }
}
