namespace Material.Application.MaterialIntegrationEventServices;

using System.Threading.Tasks;

using Core.EventBus.Events;

public interface IMaterialIntegrationEventService
{
    Task PublishThroughEventBusAsync(IntegrationEvent evt);
}
