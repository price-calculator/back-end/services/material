namespace Material.UnitTests.Application.Handlers.Queries;

using System;
using System.Linq;
using System.Threading.Tasks;

using AutoFixture;

using Material.Application.Handlers.Queries;
using Material.Application.Specifications;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class GetByNameQueryHandlerTests
{
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private GetByNameQueryHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new GetByNameQueryHandler(_materialRepositoryMock.Object, _userAccessorMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntities = _fixture.CreateMany<MaterialEntity>().ToArray();
        var getByNameQuery = _fixture.Create<GetByNameQuery>();

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.FindMaterialsAsync(It.IsAny<MaterialsLikeNameWithUsernameSpecification>())).ReturnsAsync(materialEntities);

        // Act
        var found = await _sut.Handle(getByNameQuery, default);

        // Assert
        found.ShouldNotBeNull();
        found.Length.ShouldBe(3);

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialsDoesNotExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var getByNameQuery = _fixture.Create<GetByNameQuery>();

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.FindMaterialsAsync(It.IsAny<MaterialsLikeNameWithUsernameSpecification>())).ReturnsAsync(Array.Empty<MaterialEntity>());

        // Act
        var found = await _sut.Handle(getByNameQuery, default);

        // Assert
        found.ShouldNotBeNull();
        found.ShouldBeEmpty();

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }
}
