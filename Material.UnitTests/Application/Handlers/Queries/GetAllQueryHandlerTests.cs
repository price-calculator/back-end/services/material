namespace Material.UnitTests.Application.Handlers.Queries;

using System;
using System.Linq;
using System.Threading.Tasks;

using AutoFixture;

using Material.Application.Handlers.Queries;
using Material.Application.Models;
using Material.Application.Specifications;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class GetAllQueryHandlerTests
{
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();

    private GetAllQueryHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new GetAllQueryHandler(_materialRepositoryMock.Object, _userAccessorMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var total = _fixture.Create<int>();
        var paginationParams = new PaginationParams(0, 2);
        var materialEntities = _fixture.CreateMany<MaterialEntity>().ToArray();

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.CountMaterialsAsync(username)).ReturnsAsync(total);

        _materialRepositoryMock.Setup(o => o.FindMaterialsAsync(It.IsAny<MaterialsWithUsernameSpecification>())).ReturnsAsync(materialEntities);

        // Act
        var found = await _sut.Handle(new GetAllQuery(paginationParams), default);

        // Assert
        found.ShouldNotBeNull();
        found.Total.ShouldBe(total);
        found.Materials.ShouldNotBeEmpty();
        found.Materials.Length.ShouldBe(3);

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialsDoesNotExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var paginationParams = new PaginationParams(0, 2);
        var materialEntities = _fixture.CreateMany<MaterialEntity>().ToArray();

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.CountMaterialsAsync(username)).ReturnsAsync(0);

        _materialRepositoryMock.Setup(o => o.FindMaterialsAsync(It.IsAny<MaterialsWithUsernameSpecification>())).ReturnsAsync(Array.Empty<MaterialEntity>());

        // Act
        var found = await _sut.Handle(new GetAllQuery(paginationParams), default);

        // Assert
        found.ShouldNotBeNull();
        found.Materials.ShouldBeEmpty();
        found.Total.ShouldBe(0);

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }
}
