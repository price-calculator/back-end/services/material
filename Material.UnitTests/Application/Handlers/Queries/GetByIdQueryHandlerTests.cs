namespace Material.UnitTests.Application.Handlers.Queries;

using System.Threading.Tasks;

using AutoFixture;

using Material.Application.Exceptions;
using Material.Application.Handlers.Queries;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;

using Material.Application.Specifications;
using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class GetByIdQueryHandlerTests
{
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();

    private GetByIdQueryHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new GetByIdQueryHandler(_materialRepositoryMock.Object, _userAccessorMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var getByIdQuery = new GetByIdQuery(materialEntity.Id);

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        // Act
        var found = await _sut.Handle(getByIdQuery, default);

        // Assert
        found.ShouldNotBeNull();

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialDoesNotExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var getByIdQuery = _fixture.Create<GetByIdQuery>();

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => _sut.Handle(getByIdQuery, default));

        _userAccessorMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }
}
