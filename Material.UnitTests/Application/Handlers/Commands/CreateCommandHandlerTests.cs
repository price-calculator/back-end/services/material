namespace Material.UnitTests.Application.Handlers.Commands;

using System;
using System.Threading.Tasks;

using AutoFixture;

using AutoMapper;

using Common.Events.Models;

using Core.Domain.Models;

using Material.Application.Exceptions;
using Material.Application.Handlers.Commands;
using Material.Application.MaterialIntegrationEventServices;
using Material.Application.Specifications;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;


using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class CreateCommandHandlerTests
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Loose);
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Mock<IMapper> _mapperMock = new(MockBehavior.Strict);
    private readonly Mock<IMaterialIntegrationEventService> _materialIntegrationEventServiceMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private CreateCommandHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new CreateCommandHandler(_materialRepositoryMock.Object, _userAccessorMock.Object, _materialIntegrationEventServiceMock.Object, _mapperMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var createCommand = new CreateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        _mapperMock.Setup(x => x.Map<CreateCommand, MaterialEntity>(createCommand)).Returns(materialEntity);

        _materialRepositoryMock.Setup(x => x.Add(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        _materialIntegrationEventServiceMock.Setup(o => o.PublishThroughEventBusAsync(It.Is<MaterialCreatedIntegrationEvent>(x => x.MaterialId == materialEntity.Id))).Returns(Task.CompletedTask);

        // Act
        var created = await _sut.Handle(createCommand, default);

        // Assert
        created.ShouldSatisfyAllConditions(
            () => created.Id.ShouldBe(materialEntity.Id),
            () => created.Name.ShouldBe(materialEntity.Name),
            () => created.MeasurementUnit.Id.ShouldBe(MeasurementUnit.Units.Id),
            () => created.MeasurementUnit.Name.ShouldBe(MeasurementUnit.Units.Name),
            () => created.MeasurementValue.ShouldBe(materialEntity.MeasurementValue),
            () => created.Price.ShouldBe(materialEntity.Price)
        );

        _userAccessorMock.VerifyAll();
        _mapperMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialAlreadyExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var createCommand = new CreateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => _sut.Handle(createCommand, default));

        _userAccessorMock.VerifyAll();
        _mapperMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialProblemSavingChanges_Fail()
    {
        // Arrange// Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var createCommand = new CreateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        _mapperMock.Setup(x => x.Map<CreateCommand, MaterialEntity>(createCommand)).Returns(materialEntity);

        _materialRepositoryMock.Setup(x => x.Add(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(createCommand, default));

        _userAccessorMock.VerifyAll();
        _mapperMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }
}
