namespace Material.UnitTests.Application.Specifications;

using System.Collections.Generic;
using System.Reflection;

using Core.Domain.Models;

using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

[TestFixture]
public class SpecificationFixture
{
    protected IList<MaterialEntity> materials = new List<MaterialEntity>();

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        var material = new MaterialEntity("Envelope", 1, 50, 6.99M, "John");
        SetMaterialId(material, 1);
        materials.Add(material);

        material = new MaterialEntity("Envelope", 1, 75, 7.99M, "Mary");
        SetMaterialId(material, 2);
        materials.Add(material);

        material = new MaterialEntity("Paper", 1, 250, 2.56M, "Mary");
        SetMaterialId(material, 3);
        materials.Add(material);

        material = new MaterialEntity("Stamp", 1, 25, 12.00M, "John");
        SetMaterialId(material, 4);
        materials.Add(material);
    }

    private static void SetMaterialId(MaterialEntity material, int id) =>
        typeof(Entity).GetProperty("Id").SetValue(material, id);
}
