namespace Material.UnitTests.Application.Mappers;

using AutoFixture;

using AutoMapper;

using Material.Application.Handlers.Commands;
using Material.Application.Mappers;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class MappingProfileTests
{
    private readonly Fixture _fixture = new();
    private IMapper _mapper;

    [SetUp]
    public void SetUp()
    {
        var config = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>());
        _mapper = config.CreateMapper();
    }

    [Test]
    public void MappingProfile_Map_Success()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.MeasurementUnit = MeasurementUnit.Grams.Name;

        // Act
        var material = _mapper.Map<CreateCommand, MaterialEntity>(createCommand);

        // Assert
        material.ShouldSatisfyAllConditions(
            () => material.Name.ShouldBe(createCommand.Name),
            () => material.MeasurementUnitId.ShouldBe(MeasurementUnit.Grams.Id),
            () => material.MeasurementValue.ShouldBe(createCommand.MeasurementValue),
            () => material.Price.ShouldBe(createCommand.Price),
            () => material.UserName.ShouldBe(default),
            () => material.Version.ShouldBe(0));
    }
}
