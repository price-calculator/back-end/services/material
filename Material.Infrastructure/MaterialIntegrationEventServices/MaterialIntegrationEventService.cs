namespace Material.Infrastructure.MaterialIntegrationEventServices;

using System;
using System.Threading.Tasks;

using Core.EventBus.Bus;
using Core.EventBus.Events;

using Material.Application.MaterialIntegrationEventServices;

using Microsoft.Extensions.Logging;

public class MaterialIntegrationEventService : IMaterialIntegrationEventService
{

    private readonly ILogger<MaterialIntegrationEventService> _logger;
    private readonly IEventBus _eventBus;

    public MaterialIntegrationEventService(IEventBus eventBus, ILogger<MaterialIntegrationEventService> logger)
    {
        _eventBus = eventBus;
        _logger = logger;
    }

    public Task PublishThroughEventBusAsync(IntegrationEvent evt)
    {
        try
        {
            _logger.LogInformation("----- Publishing event: {EventId} from {AssemblyName} - {evt}", evt.Id, this.GetType().Assembly.GetName(), evt);
            _eventBus.Publish(evt);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "ERROR Publishing event: {EventId} from {AssemblyName} - {evt}", evt.Id, this.GetType().Assembly.GetName(), evt);
        }

        return Task.CompletedTask;
    }
}
