namespace Material.Domain.Models.MaterialAggregate;

using Core.Domain.Models;

public class MaterialEntity : Entity, IAgreggateRoot
{
    public string Name { get; set; }
    public MeasurementUnit MeasurementUnit { get; set; }
    public int MeasurementUnitId { get; set; }
    public int MeasurementValue { get; set; }
    public decimal Price { get; set; }
    public string UserName { get; set; }

    public MaterialEntity(string name, int measurementUnitId, int measurementValue, decimal price)
    {
        Name = name;
        MeasurementUnitId = measurementUnitId;
        MeasurementValue = measurementValue;
        Price = price;
    }

    public MaterialEntity(string name, int measurementUnitId, int measurementValue, decimal price, string username)
    {
        Name = name;
        MeasurementUnitId = measurementUnitId;
        MeasurementValue = measurementValue;
        Price = price;
        UserName = username;
    }

    public void IncreaseVersion() => Version++;
}
