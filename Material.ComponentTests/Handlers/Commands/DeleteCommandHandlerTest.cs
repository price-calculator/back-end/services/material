namespace Material.ComponentTests.Handlers;

using System;
using System.Threading.Tasks;

using AutoFixture;

using Customizations;

using Material.Application.Exceptions;
using Material.Application.Handlers.Commands;
using Material.Application.Specifications;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using static Testing;

using Shouldly;

[TestFixture]
public class DeleteCommandHandlerTest : TestBase
{
    private readonly Fixture _fixture = new();
    [SetUp]
    public void TestSetUp() => _fixture.Customize(new JohnUnitMaterialCustomization());

    [Test]
    public async Task DeleteCommandHandler_HappyFlow_MaterialCreated()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        var deleteCommand = new DeleteCommand(created.Id);

        // Act
        await SendAsync(deleteCommand);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));
        found.ShouldBeNull();

        (await ShouldHaveReceivedMaterialDeletedIntegrationEventHandler(material.Name, TimeSpan.FromSeconds(3))).ShouldBeTrue();
    }

    [Test]
    public async Task DeleteCommandHandler_MaterialDoesNotExists_ThrowsMaterialException()
    {
        // Arrange
        var deleteCommand = new DeleteCommand(1);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => SendAsync(deleteCommand));
    }
}
