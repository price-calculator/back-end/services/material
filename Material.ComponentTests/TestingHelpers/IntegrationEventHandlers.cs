namespace Material.ComponentTests.TestingHelpers;

using System.Collections.Concurrent;
using System.Threading.Tasks;

using Common.Events.Models;

using Core.EventBus.Events;
using Core.EventBus.Handlers;

public abstract class IntegrationEventHandlerBase<T> : IIntegrationEventHandler<T> where T : IntegrationEvent
{
    private readonly ConcurrentBag<T> _bag;
    protected IntegrationEventHandlerBase(ConcurrentBag<T> bag) => _bag = bag;
    public Task Handle(T @event)
    {
        _bag.Add(@event);
        return Task.CompletedTask;
    }
}

public class MaterialCreatedIntegrationEventHandler : IntegrationEventHandlerBase<MaterialCreatedIntegrationEvent>
{
    public MaterialCreatedIntegrationEventHandler(ConcurrentBag<MaterialCreatedIntegrationEvent> bag) : base(bag)
    {

    }

    public new Task Handle(MaterialCreatedIntegrationEvent @event) => base.Handle(@event);
}

public class MaterialUpdatedIntegrationEventHandler : IntegrationEventHandlerBase<MaterialUpdatedIntegrationEvent>
{
    public MaterialUpdatedIntegrationEventHandler(ConcurrentBag<MaterialUpdatedIntegrationEvent> bag) : base(bag)
    {

    }

    public new Task Handle(MaterialUpdatedIntegrationEvent @event) => base.Handle(@event);
}

public class MaterialDeletedIntegrationEventHandler : IntegrationEventHandlerBase<MaterialDeletedIntegrationEvent>
{
    public MaterialDeletedIntegrationEventHandler(ConcurrentBag<MaterialDeletedIntegrationEvent> bag) : base(bag)
    {

    }

    public new Task Handle(MaterialDeletedIntegrationEvent @event) => base.Handle(@event);
}
