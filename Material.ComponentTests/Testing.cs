namespace Material.ComponentTests;

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using Core.Domain.Models;
using Core.EventBus.Bus;

using Material.API;
using Material.ComponentTests.Extensions;
using Material.ComponentTests.TestingHelpers;
using Material.Data.Context;
using Material.Domain.Models.MaterialAggregate;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NUnit.Framework;

using Respawn;
using Respawn.Graph;

[SetUpFixture]
public class Testing
{
    private static IConfiguration _configuration;
    private static Checkpoint _checkpoint;
    private static IServiceScopeFactory _scopeFactory;

    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables();
        _configuration = builder.Build();

        SetupEnvironmentVariables();

        var services = new ServiceCollection().AddLogging(logging => logging.AddConsole());
        var startup = new Startup(_configuration);
        startup.ConfigureServices(services);
        services.AddWebHostEnvironment();
        services.AddMockedCustomServices();
        services.AddSingleton(_ => new ConcurrentBag<MaterialCreatedIntegrationEvent>());
        services.AddSingleton(_ => new ConcurrentBag<MaterialUpdatedIntegrationEvent>());
        services.AddSingleton(_ => new ConcurrentBag<MaterialDeletedIntegrationEvent>());
        services.AddSingleton<MaterialCreatedIntegrationEventHandler>();
        services.AddSingleton<MaterialUpdatedIntegrationEventHandler>();
        services.AddSingleton<MaterialDeletedIntegrationEventHandler>();

        _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

        await InitializeDatabaseAsync();
        await InitializeMessageConsumersAsync();
        _checkpoint = new Checkpoint
        {
            TablesToIgnore = new Table[] { "__EFMigrationsHistory", "measurements" }
        };
    }

    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        await TearDownDatabaseAsync();
        SetupEnvironmentVariables(up: false);
    }

    private static void SetupEnvironmentVariables(bool up = true)
    {
        Environment.SetEnvironmentVariable("JWT_KEY", up ? "ThisIsTheSecretJwt" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_HOSTNAME", up ? "localhost" : null);
        Environment.SetEnvironmentVariable("MSSQL_CONNECTIONSTRING", up ? "Server=localhost,1433;Database=test;User=SA;Password=Password_123;MultipleActiveResultSets=true;Encrypt=False" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_USERNAME", up ? "guest" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_PASSWORD", up ? "guest" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_RETRYCOUNT", up ? "5" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_SUBSCRIPTION_CLIENTNAME", up ? "price_calculator" : null);
    }

    private static async Task InitializeMessageConsumersAsync(CancellationToken cancellationToken = default)
    {
        await new TaskFactory().StartNew(() =>
        {
            using var scope = _scopeFactory.CreateScope();
            var services = scope.ServiceProvider;
            var eventBus = services.GetRequiredService<IEventBus>();

            eventBus.Subscribe<MaterialCreatedIntegrationEvent, MaterialCreatedIntegrationEventHandler>();
            eventBus.Subscribe<MaterialUpdatedIntegrationEvent, MaterialUpdatedIntegrationEventHandler>();
            eventBus.Subscribe<MaterialDeletedIntegrationEvent, MaterialDeletedIntegrationEventHandler>();
        }, cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Current);

    }

    private static async Task InitializeDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<MaterialContext>();
            await context.ApplyMigrationsAsync().ConfigureAwait(false);
            await context.ApplySeedsAsync(services).ConfigureAwait(false);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private static async Task TearDownDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetService<MaterialContext>();
            await context.Database.EnsureDeletedAsync();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
    {
        using var scope = _scopeFactory.CreateScope();
        var mediator = scope.ServiceProvider.GetService<IMediator>();

        return await mediator.Send(request);
    }

    public static async Task ResetState()
    {
        await _checkpoint.Reset(Environment.GetEnvironmentVariable("MSSQL_CONNECTIONSTRING"));
    }

    public static async Task<MaterialEntity> AddMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IMaterialRepository>();
            var created = repository.Add(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return created;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> UpdateMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<MaterialContext>();
            var repository = services.GetRequiredService<IMaterialRepository>();
            context.Entry(material).State = EntityState.Modified;
            var updated = repository.Update(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return updated;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> DeleteMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<MaterialContext>();
            var repository = services.GetRequiredService<IMaterialRepository>();
            context.Entry(material).State = EntityState.Modified;
            var updated = repository.Delete(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return updated;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> FindMaterialAsync(ISpecification<MaterialEntity> specification)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IMaterialRepository>();
            var material = await repository.FindMaterialAsync(specification);

            return material;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity[]> FindMaterialsAsync(ISpecification<MaterialEntity> specification)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IMaterialRepository>();
            var materials = await repository.FindMaterialsAsync(specification);

            return materials;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static void ExecuteSqlInterpolated(FormattableString sql)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<MaterialContext>();
            context.Database.ExecuteSqlInterpolated(sql);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private static T Execute<T>(Func<T> function, TimeSpan timeout, Func<T> onTimeout)
    {
        var task = Task.Run(function);
        if (task.Wait(timeout))
        {
            return task.Result;
        }
        else
        {
            return onTimeout();
        }
    }

    public static Task<bool> ShouldHaveReceivedMaterialCreatedIntegrationEventHandler(string name, TimeSpan timeout)
    {
        using var scope = _scopeFactory.CreateScope();
        var result = false;
        var services = scope.ServiceProvider;
        try
        {
            var bag = services.GetRequiredService<ConcurrentBag<MaterialCreatedIntegrationEvent>>();

            result = Execute<bool>(
                () =>
                {
                    var @event = default(MaterialCreatedIntegrationEvent);
                    do
                    {
                        Thread.Sleep(200);
                        @event = bag.FirstOrDefault(e => e.Name == name);
                    } while (@event == null);

                    return bag.TryTake(out @event);
                },
                timeout,
                () => false);
        }
        catch (Exception)
        {
            throw;
        }

        return Task.FromResult(result);
    }

    public static Task<bool> ShouldHaveReceivedMaterialUpdatedIntegrationEventHandler(string name, TimeSpan timeout)
    {
        using var scope = _scopeFactory.CreateScope();
        var result = false;
        var services = scope.ServiceProvider;

        try
        {
            var bag = services.GetRequiredService<ConcurrentBag<MaterialUpdatedIntegrationEvent>>();

            result = Execute<bool>(
                () =>
                {
                    var @event = default(MaterialUpdatedIntegrationEvent);
                    do
                    {
                        Thread.Sleep(200);
                        @event = bag.FirstOrDefault(e => e.Name == name);
                    } while (@event == null);

                    return bag.TryTake(out @event);
                },
                timeout,
                () => false);
        }
        catch (Exception)
        {
            throw;
        }

        return Task.FromResult(result);
    }

    public static Task<bool> ShouldHaveReceivedMaterialDeletedIntegrationEventHandler(string name, TimeSpan timeout)
    {
        using var scope = _scopeFactory.CreateScope();
        var result = false;
        var services = scope.ServiceProvider;

        try
        {
            var bag = services.GetRequiredService<ConcurrentBag<MaterialDeletedIntegrationEvent>>();

            result = Execute<bool>(
                () =>
                {

                    var @event = default(MaterialDeletedIntegrationEvent);
                    do
                    {
                        Thread.Sleep(200);
                        @event = bag.FirstOrDefault(e => e.Name == name);
                    } while (@event == null);

                    return bag.TryTake(out @event);
                },
                timeout,
                () => false);
        }
        catch (Exception)
        {
            throw;
        }

        return Task.FromResult(result);
    }
}
